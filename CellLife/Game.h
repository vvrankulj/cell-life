#pragma once

struct Enemy{
	int sprite;
	float currentSpeedX;
	float currentSpeedY;
	int directionX;
	int directionY;
	float rotationSpeed;
	//Pause vars
	float pauseSpeedX;
	float pauseSpeedY;
	int pauseDirectionX;
	int pauseDirectionY;
	float pauseRotationSpeed;

	bool isAlive;
};

struct Character{
	int sprite;
	float currentSpeedX;
	float currentSpeedY;
	float rotationSpeed;
	//Pause vars
	float pauseSpeedX;
	float pauseSpeedY;
	float pauseRotationSpeed;

	bool isAlive;
};

class Game
{
public:
	void LoadGameImages(void);
	void StartGame(void);
	void StopGame(void);
	void PauseGame(void);
	void NextLevel(void);
	Enemy enemy;
	Character character;

private:
	void CreateCharacter(void);
	void CreateEnemies(void);
	void CreateEnvironment(void);
	void UpdateCharacterPositions(void);
	void UpdateEnemyPositions(void);
	void DestroyCharacter(void);
	void DestroyEnemy(void);
	void DestroyEnvironment(void);
	void MainReset(void);
	void CreateParticles(void);
};



