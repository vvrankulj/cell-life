#pragma once
class MultiplayerGame
{
public:
	void LoadMultiplayerButtonImages(void);
	void StartMultiplayerGame(void);
	void EndMultiplayerGame(void);

private:
	void StartHostServer(void);
	void JoinHostedServer(void);
	void CreatePlayer(void);
	void DeletePlayer(void);
};

