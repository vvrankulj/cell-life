#include "MultiplayerGame.h"
#include "Main.h"
#include "agk.h"
#include <String>

using namespace std;

int NetworkId;
bool multiplayerMenuCreated = false;
int MultiplayerBackButtonImage;
string ServerName = "";
const char * c_serverName = ServerName.c_str();
bool serverNameRequired = false;

void MultiplayerGame::LoadMultiplayerButtonImages(){
	MultiplayerBackButtonImage = agk::LoadImage("BackButton.png");
}

void _CreateMultiplayerMenu(void){
	if(!multiplayerMenuCreated){
		agk::AddVirtualButton(5,120,100,60);
		agk::AddVirtualButton(6,120,200,60);
		agk::AddVirtualButton(7,120,300,60);
		agk::SetVirtualButtonImageUp(5,0);
		agk::SetVirtualButtonImageDown(5,0);
		agk::SetVirtualButtonImageUp(6,0);
		agk::SetVirtualButtonImageDown(6,0);
		agk::SetVirtualButtonImageUp(7,MultiplayerBackButtonImage);
		agk::SetVirtualButtonImageDown(7,MultiplayerBackButtonImage);
		multiplayerMenuCreated = true;
	}
}

void _DeleteMultiplayerMenu(void){
	if(multiplayerMenuCreated){
		agk::DeleteVirtualButton(5);
		agk::DeleteVirtualButton(6);
		agk::DeleteVirtualButton(7);
		multiplayerMenuCreated = false;
	}
}

void MultiplayerGame::StartMultiplayerGame(){
	if(!multiplayerMenuCreated){
		_CreateMultiplayerMenu();
	}

	agk::Print(c_serverName);

	if(agk::GetVirtualButtonPressed(5)==1){
		agk::StartTextInput();
		serverNameRequired = true;
		StartHostServer();
	}
	if(agk::GetVirtualButtonPressed(6)==1){
		JoinHostedServer();
	}
	if(agk::GetVirtualButtonPressed(7)==1){
		_DeleteMultiplayerMenu();
		MenuMode = 0;
		g_GameMode = 0;
	}

	if(serverNameRequired){
		if(agk::GetTextInputCompleted()==1){
			ServerName = agk::GetTextInput();
			serverNameRequired = false;
		}
	}
}

void MultiplayerGame::EndMultiplayerGame(){

}

void MultiplayerGame::StartHostServer(){
	c_serverName = ServerName.c_str();
	NetworkId = agk::HostNetwork(c_serverName,"Player1",1025);
}

void MultiplayerGame::JoinHostedServer(){

	NetworkId = agk::JoinNetwork("CellLifeServer","Player2");
}


