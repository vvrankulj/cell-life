#include "Weapons.h"
#include "Game.h"
#include "agk.h"

Character _character;

int shrinkingRing = 0;
int shrinkButtonImage = 0;
bool weaponButtonCreated = false;
bool weaponFired = false;
int fireButton = 10;


void Weapons::LoadFireButtonImage(){
	shrinkButtonImage = agk::LoadImage("ShrinkWeapon.png");
	shrinkingRing = agk::LoadImage("ShrinkRing.png");
}

void Weapons::CreateFireButton(){
	if(!weaponButtonCreated){
		agk::AddVirtualButton(fireButton,600,440,80);
		agk::SetVirtualButtonImageUp(fireButton,shrinkButtonImage);
		agk::SetVirtualButtonImageDown(fireButton,shrinkButtonImage);
		weaponButtonCreated = true;
	}
}

void Weapons::DeleteFireButton(){
	if(weaponButtonCreated){
		agk::DeleteVirtualButton(fireButton);
		weaponButtonCreated = false;
	}
}
int weaponFiring = 0;
bool burstCreated = false;
int explosion = 0;

void Weapons::FireShrinkingRing(float x, float y, float width, float height){

	if(!burstCreated){
		explosion = agk::CreateSprite(shrinkingRing);
		agk::SetSpriteSize(explosion,width, height);
		agk::SetSpritePosition(explosion,x,y);
		burstCreated = true;
	}

	agk::SetSpriteSize(explosion, agk::GetSpriteWidth(explosion) + 10.0, agk::GetSpriteHeight(explosion) + 10.0);
	agk::SetSpritePosition(explosion, agk::GetSpriteX(explosion) - 5.0,agk::GetSpriteY(explosion) - 5.0);
	agk::SetSpriteColorAlpha(explosion,agk::GetSpriteColorAlpha(explosion) - 12.0 );

	if(agk::GetSpriteColorAlpha(explosion) <= 5){
		agk::DeleteSprite(explosion);
		weaponFiring = 0;
		burstCreated = false;
	}
}
