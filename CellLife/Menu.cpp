#include "Menu.h"
#include "agk.h"

bool mainMenuCreated = false;
bool playMenuCreated = false;
bool dialogMenuCreated = false;
bool achievmentsMenuCreated = false;
bool settingsMenuCreated = false;

int background1,
	background2,
	background3,
	background4,
	background5,
	background6,
	background7,
	background8,
	background9,
	background10;

int PlayButtonImage, 
	OptionsButtonImage, 
	CreditsButtonImage, 
	ExitButtonImage, 
	SinglePlayerImage, 
	MultiPlayerImage, 
	BackButtonImage, 
	NoButtonImage, 
	YesButtonImage;

int imageIndex = 1;
bool backgroundSet = true;

void Menu::LoadMenuButtonImages(){
	PlayButtonImage = agk::LoadImage("PlayButton.png");
	OptionsButtonImage = agk::LoadImage("OptionsButton.png");
	CreditsButtonImage = agk::LoadImage("CreditsButton.png");
	ExitButtonImage = agk::LoadImage("ExitButton.png");
	SinglePlayerImage = agk::LoadImage("SingleButton.png");
	MultiPlayerImage = agk::LoadImage("MultiButton.png");
	BackButtonImage = agk::LoadImage("BackButton.png");
	NoButtonImage = agk::LoadImage("NoButton.png");
	YesButtonImage = agk::LoadImage("YesButton.png");
}
void Menu::LoadBackgroundImages(){
	background1 = agk::LoadImage("Background.png");
	background2 = agk::LoadImage("Background2.png");
	background3 = agk::LoadImage("Background3.png");
	background4 = agk::LoadImage("Background4.png");
	background5 = agk::LoadImage("Background5.png");
	background6 = agk::LoadImage("Background6.png");
	background7 = agk::LoadImage("Background7.png");
	background8 = agk::LoadImage("Background8.png");
	background9 = agk::LoadImage("Background9.png");
	background10 = agk::LoadImage("Background10.png");
}

void changeBackground(int image){

	if(agk::GetSpriteColorAlpha(101) > 0 && !backgroundSet){
		agk::SetSpriteColorAlpha(101, agk::GetSpriteColorAlpha(101) - 0.1);
	}else if(agk::GetSpriteColorAlpha(101) == 0 && !backgroundSet){
		agk::SetSpriteImage(101,image);
		backgroundSet = true;
	}

	if(backgroundSet){
		agk::SetSpriteColorAlpha(101,50);
	}

}
void Menu::ChangeBackgroundImage(){
	int background = 0;
	switch(imageIndex){
	case 1:
		background = background1;
		break;
	case 2:
		background = background2;
		break;
	case 3:
		background = background3;
		break;
	case 4:
		background = background4;
		break;
	case 5:
		background = background5;
		break;
	case 6:
		background = background6;
		break;
	case 7:
		background = background7;
		break;
	case 8:
		background = background8;
		break;
	case 9:
		background = background9;
		break;
	case 10:
		background = background10;
		break;
	default:
		imageIndex = 1;
		background = background5;
		break;
	}
	if((agk::GetMilliseconds()/100)%200 == 0){
		backgroundSet = false;
	}
	if(!backgroundSet){
		changeBackground(background);
		imageIndex ++;
	}
};
//Private functions
void Menu::CreateAchievmentMenu(){
	if(!achievmentsMenuCreated){
		agk::CreateText(1, "Coffee stains studio");
		agk::SetTextColor(1,250,50,100,255);
		agk::SetTextSize(1,24);
		agk::SetTextPosition(1,150,220);
		agk::AddVirtualButton(7,120,400,60);
		agk::SetVirtualButtonImageUp(7,BackButtonImage);
		agk::SetVirtualButtonImageDown(7,BackButtonImage);
		achievmentsMenuCreated = true;
	}
}

void Menu::CreateMainMenu(){
	if(!mainMenuCreated){
		agk::AddVirtualButton(1,120,100,60);
		agk::AddVirtualButton(2,120,200,60);
		agk::AddVirtualButton(3,120,300,60);
		agk::AddVirtualButton(4,120,400,60);
		agk::SetVirtualButtonImageUp(1,PlayButtonImage);
		agk::SetVirtualButtonImageDown(1,PlayButtonImage);
		agk::SetVirtualButtonImageUp(2,OptionsButtonImage);
		agk::SetVirtualButtonImageDown(2,OptionsButtonImage);
		agk::SetVirtualButtonImageUp(3,CreditsButtonImage);
		agk::SetVirtualButtonImageDown(3,CreditsButtonImage);
		agk::SetVirtualButtonImageUp(4,ExitButtonImage);
		agk::SetVirtualButtonImageDown(4,ExitButtonImage);
		mainMenuCreated = true;
	}
}

void Menu::CreatePlayMenu(){
	if(!playMenuCreated){
		agk::AddVirtualButton(5,120,100,60);
		agk::AddVirtualButton(6,120,200,60);
		agk::AddVirtualButton(7,120,300,60);
		agk::SetVirtualButtonImageUp(5,SinglePlayerImage);
		agk::SetVirtualButtonImageDown(5,SinglePlayerImage);
		agk::SetVirtualButtonImageUp(6,MultiPlayerImage);
		agk::SetVirtualButtonImageDown(6,MultiPlayerImage);
		agk::SetVirtualButtonImageUp(7,BackButtonImage);
		agk::SetVirtualButtonImageDown(7,BackButtonImage);
		playMenuCreated = true;
	}
}

void Menu::CreateSettingsMenu(){

	if(!settingsMenuCreated){
		agk::AddVirtualButton(5,120,100,50);
		agk::AddVirtualButton(6,120,200,50);
		agk::AddVirtualButton(7,120,300,60);
		agk::SetVirtualButtonText(5,"Music On/Off");
		agk::SetVirtualButtonText(6,"SoundFX On/Off");
		agk::SetVirtualButtonImageUp(7,BackButtonImage);
		agk::SetVirtualButtonImageDown(7,BackButtonImage);
		settingsMenuCreated = true;
	}

}

void Menu::CreateDialogMenu(){
	if(!dialogMenuCreated){
		agk::CreateText(1,"Do you realy want to quit?");
		agk::SetTextColor(1,250,50,100,255);
		agk::SetTextSize(1,24);
		agk::SetTextPosition(1,130,220);
		agk::AddVirtualButton(8,80,400,60);
		agk::AddVirtualButton(9,580,400,60);
		agk::SetVirtualButtonImageUp(8,YesButtonImage);
		agk::SetVirtualButtonImageDown(8,YesButtonImage);	
		agk::SetVirtualButtonImageUp(9,NoButtonImage);
		agk::SetVirtualButtonImageDown(9,NoButtonImage);
		dialogMenuCreated = true;
	}
}

void Menu::DeleteAchievmentMenu(){
	agk::DeleteText(1);
	agk::DeleteVirtualButton(7);
	achievmentsMenuCreated = false;
}

void Menu::DeleteMainMenu(){
	agk::DeleteVirtualButton(1);
	agk::DeleteVirtualButton(2);
	agk::DeleteVirtualButton(3);
	agk::DeleteVirtualButton(4);
	mainMenuCreated = false;
}

void Menu::DeletePlayMenu(){
	agk::DeleteVirtualButton(5);
	agk::DeleteVirtualButton(6);
	agk::DeleteVirtualButton(7);
	playMenuCreated = false;
}

void Menu::DeleteSettingMenu(){
	agk::DeleteVirtualButton(5);
	agk::DeleteVirtualButton(6);
	agk::DeleteVirtualButton(7);
	settingsMenuCreated = false;
}

void Menu::DeleteDialogMenu(){
	agk::DeleteText(1);
	agk::DeleteVirtualButton(8);
	agk::DeleteVirtualButton(9);
	dialogMenuCreated = false;
}

