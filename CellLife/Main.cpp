// Includes, namespace and prototypes
#include "Main.h"
#include "Game.h"
#include "Weapons.h"
#include "Menu.h"
#include "MultiplayerGame.h"

using namespace AGK;
app App;

Game game;
Menu menu;
Weapons weapons;
MultiplayerGame multiplayerGame;

#ifdef IDE_ANDROID
extern void exit(int);
#endif

bool app::did_start = false;
bool app::did_end = false;

void OptionsMenuHandler();
void AchievmentsMenuHandler();
void QuitMenuHandler();
void PlayMenuHandler();

int background = 0;

//Game Modes:
// 0 ------- Main Menu Mode
// 1 ------- Play Game Mode
int g_GameMode = 0;
//PlayGamemodes:
// 0 ------- game stoped
// 1 ------- game playing
// 2 ------- game paused - quit menu
// 3 ------- next level menu 
// 4 ------- quit game (g_GameMode = 0)
int PlayGameMode = 0;
//MenuMode
// 0 ------- play menu
// 1 ------- options menu
// 2 ------- credits menu
// 3 ------- exit menu
int MenuMode = -1;

// Begin app, called once at the start
void app::Begin( void )
{
#ifdef IDE_MAC
	// use device resolution
	agk::SetVirtualResolution(m_DeviceWidth,m_deviceHeight);
#endif
	agk::SetSyncRate( 60, 1 );
	menu.LoadBackgroundImages();
	background = agk::LoadImage("Background10.png");
	agk::CreateSprite(101,background);
	agk::SetSpriteColorAlpha(101,50);
	agk::SetSpritePosition(101,0, 0);
	agk::SetSpriteSize(101,agk::GetDeviceWidth(), agk::GetDeviceHeight());
	agk::SetVirtualResolution(agk::GetDeviceWidth(),agk::GetDeviceHeight());
	agk::SetDisplayAspect(4.0 / 3.0);
	agk::SetPhysicsWallBottom(1);
	agk::SetPhysicsWallTop(1);
	agk::SetPhysicsWallLeft(1);
	agk::SetPhysicsWallRight(1);
	game.LoadGameImages();
	menu.LoadMenuButtonImages();
	weapons.LoadFireButtonImage();
	multiplayerGame.LoadMultiplayerButtonImages();
}

//g_GameMode = 0
//Provides menu
void MainMenuHandler(){
	menu.ChangeBackgroundImage();
	if(agk::GetVirtualButtonPressed(1)==1){
		MenuMode = 0;
	}else if(agk::GetVirtualButtonPressed(2)==1){
		MenuMode = 1;
	}else if(agk::GetVirtualButtonPressed(3)==1){
		MenuMode = 2;
	}else if(agk::GetVirtualButtonPressed(4)==1){
		MenuMode = 3;
	}

	switch(MenuMode){
	case -1:
		menu.CreateMainMenu();
		break;
	case 0:
		PlayMenuHandler();
		break;
	case 1:
		OptionsMenuHandler();
		break;
	case 2:
		AchievmentsMenuHandler();
		break;
	case 3:
		QuitMenuHandler();
		break;
	default:
		break;
	}
}

//MenuMode = 1
void OptionsMenuHandler(){
	menu.DeleteMainMenu();
	menu.CreateSettingsMenu();
		if(agk::GetVirtualButtonPressed(5)==1){
		agk::Print("Music Off/On");
	}else if(agk::GetVirtualButtonPressed(6)==1){
		agk::Print("SoundFx Off/On");
	}else if(agk::GetVirtualButtonPressed(7)==1){
		MenuMode = -1;
		menu.DeleteSettingMenu();
	}
}

//MenuMode = 2
void AchievmentsMenuHandler(){
	menu.DeleteMainMenu();
	menu.CreateAchievmentMenu();

	if(agk::GetVirtualButtonPressed(7)==1){
		menu.DeleteAchievmentMenu();
		MenuMode = -1;
	}
}

//MenuMode = 3
void QuitMenuHandler(){
	menu.DeleteMainMenu();
	menu.CreateDialogMenu();

	if(agk::GetVirtualButtonPressed(9)==1){
		menu.DeleteDialogMenu();
		MenuMode = -1;
	}else if(agk::GetVirtualButtonPressed(8)==1){
		menu.DeleteDialogMenu();
		exit(1);
	}
}

void PlayMenuHandler(){
	menu.DeleteMainMenu();
	menu.CreatePlayMenu();
if(agk::GetVirtualButtonPressed(5)==1){
		g_GameMode = 1;
		PlayGameMode = 1;
		menu.DeletePlayMenu();
	}else if(agk::GetVirtualButtonPressed(6)==1){
		g_GameMode = 1;
		PlayGameMode = 2;
		menu.DeletePlayMenu();
	}else if(agk::GetVirtualButtonPressed(7)==1){
		MenuMode = -1;
		menu.DeletePlayMenu();
	}
}

	//g_GameMode = 1
	//Provides gameplay {start, pause, stop}
	void GameHandler()
	{
		switch(PlayGameMode){
		case 0:
			agk::Print("Game stoped");
			break;
		case 1:
			game.StartGame();
			break;
		case 2:
			multiplayerGame.StartMultiplayerGame();
			break;
		case 3:
			agk::Print("Next level menu");
			break;
		default:
			break;
		}
	}


	// Main loop, called every frame
	void app::Loop ( void )
	{
		
		// indicate started
		app::did_start = true;

		// check for ended
		if (app::did_end) return;

		switch(g_GameMode){
		case 0:
			MainMenuHandler();
			break;
		case 1:
			GameHandler();
			break;
		default:
			break;
		}

		//// check for closing
		//if (agk::GetPointerPressed())
		//{
		//	// call method to close up
		//	closeThisApp();
		//} else {
		//	
		//}

		// sync display
		agk::Sync();
	}

	// Called when the app ends
	void app::End ( void )
	{
	}

#ifdef IDE_XCODE
	// Called when the app returns from being in the background
	void app::appGetResumed( void )
	{
		// do anything that needs to be handled after being paused
		agk::Message("We have returned!");
	}
#endif

	void app::closeThisApp()
	{
		// indicate done
		app::did_end = true;

		// completely exit the app
#ifdef AGKWINDOWS
		PostQuitMessage(0);
#endif
#ifdef AGKIOS
		// forcing a quit in iOS is against recommended guidelines - use HOME button
		// the exit button is disabled on AGKIOS builds
		// but if you want to do so, this is the code
		agk::MasterReset();
		exit(1);
#endif
#ifdef IDE_ANDROID
		// similar to iOS, an exit button should not be done
		// but if you want to do so, this is the code
		agk::MasterReset();
		exit(0);
#endif
#ifdef IDE_MAC
		glfwCloseWindow();
#endif
#ifdef IDE_MEEGO
		g_appptr->quit();
#endif
#ifdef IDE_BADA
		// Bada platform has a HOME button to quit apps
		// but the END command can also quit a Bada App forcefully
		Application::GetInstance()->Terminate();
#endif
	}
