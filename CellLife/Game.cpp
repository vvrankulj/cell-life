#include "Game.h"
#include "Main.h"
#include "Weapons.h"
#include "agk.h"
#include <vector>
#include <sstream>
#include <string>
#include <string.h>
#include <stdarg.h>

using namespace std;
/////////////////////////////////////////////////////////////
/////////////////Instance Of class///////////////////////////
/////////////////////////////////////////////////////////////
Weapons weapon;

/////////////////////////////////////////////////////////////
/////////////////Game.cpp Global variables///////////////////
/////////////////////////////////////////////////////////////

int characterImage = 0;
float characterWidth = 40.0f;
float characterHeight = 40.0f;
bool characterAlive = false;
bool characterCreated = false;
bool characterMoving = false;
bool charSpeedSet = false;

int enemyImage = 0;
bool enemiesCreated = false;
int enemyCounter = 10;
vector<Enemy> enemiesSprites;
int smallestSprite = 0;

int targetImage = 0;

bool joystickCreated = false;
bool resumeCreated = false;
int level = 1;
int nextLevelButton = 9;
bool nextLevelMenuCreated = false;

bool gamePaused = false;
bool gamePlaying = false;

bool particleCreated = false;

int direction = 1;
int directionY = 1;

int PauseButtonImage, ResumeButtonImage, NextLevelButtonImage;
int ScoreCoinImage1, ScoreCoinImage2, ScoreCoinImage3, ScoreCoinImage4, ScoreCoinImage5;
int scoreText;
int totalScore;
char big_string[1024];

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////HELPER FUNCTIONS DECLARATIONS//////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

void _CheckCollision();
void _FindSmallestSprite();
void _CreateScoreAnimation();
void _PlayCoinAnimation();
void _ResumeCoinAnimation();
void _StopCoinAnimation();
void _DeleteCoinAnimation();
void _CreateScoreText();
void _UpdateScoreText(int);
void _UpdateScore(int);
const char* format_my_way(const char* fmt, ...);

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////HELPER FUNCTIONS IMPLEMENTATIONS////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

const char* format_my_way(const char* fmt, ...)
{
	// handle the input
	va_list args;
	// create our own string
	va_start(args,fmt);
	vsnprintf(big_string, 1024, fmt, args);
	va_end(args);
	// return constant
	return (const char*)big_string;
}

void _CreateScoreAnimation(){
	agk::CreateSprite ( 51, 0 );
	agk::AddSpriteAnimationFrame ( 51, ScoreCoinImage1 );
	agk::AddSpriteAnimationFrame ( 51, ScoreCoinImage2 );
	agk::AddSpriteAnimationFrame ( 51, ScoreCoinImage3 );
	agk::AddSpriteAnimationFrame ( 51, ScoreCoinImage4 );
	agk::AddSpriteAnimationFrame ( 51, ScoreCoinImage5 );
	agk::AddSpriteAnimationFrame ( 51, ScoreCoinImage4 );
	agk::AddSpriteAnimationFrame ( 51, ScoreCoinImage3 );
	agk::AddSpriteAnimationFrame ( 51, ScoreCoinImage2 );
	agk::SetSpritePosition(51,10.0,50.0);
	agk::SetSpriteSize(51,25,-1);
	agk::SetSpriteDepth(51,1);
	_CreateScoreText();
}

void _CheckCollision(){
	for(int i = 0; i<enemiesSprites.size(); i++){
		for(int j = i+1; j<enemiesSprites.size(); j++)
		{
			Enemy temp1 = enemiesSprites.at(i);
			Enemy temp2 = enemiesSprites.at(j);
			if(agk::GetSpriteCollision(temp1.sprite,temp2.sprite) == 1){
				if(agk::GetSpriteWidth(temp1.sprite) > agk::GetSpriteWidth(temp2.sprite)){
					agk::SetSpriteSize(temp1.sprite,agk::GetSpriteWidth(temp1.sprite) + agk::GetSpriteWidth(temp2.sprite)/2,  agk::GetSpriteHeight(temp1.sprite) + agk::GetSpriteHeight(temp2.sprite)/2);
					agk::DeleteSprite(temp2.sprite);
					temp2.isAlive = false;
					enemyCounter --;
					break;
				}else if (agk::GetSpriteWidth(temp1.sprite) <= agk::GetSpriteWidth(temp2.sprite)){
					agk::SetSpriteSize(temp2.sprite,agk::GetSpriteWidth(temp2.sprite) + agk::GetSpriteWidth(temp1.sprite)/2,  agk::GetSpriteHeight(temp2.sprite) + agk::GetSpriteHeight(temp1.sprite)/2);
					agk::DeleteSprite(temp1.sprite);
					temp1.isAlive = false;
					enemyCounter --;
					break;
				}
			}
		}
	}
}

void _FindSmallestSprite(){
	vector<Enemy> livingEnemies;
	for(int i = 0; i <  enemiesSprites.size(); i++){
		Enemy temp = enemiesSprites.at(i);
		if(temp.isAlive && agk::GetSpriteActive(temp.sprite)==1){
			livingEnemies.push_back(temp);
		}
	}
	smallestSprite = livingEnemies.at(0).sprite;
	if(livingEnemies.size()>0){
		for(int j = 0; j<livingEnemies.size(); j++){
			Enemy temp2 = livingEnemies.at(j);
			if(agk::GetSpriteWidth(temp2.sprite) <= agk::GetSpriteWidth(smallestSprite)){
				smallestSprite = temp2.sprite;
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////SCORE HELPER FUNCTIONS///////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

void _UpdateScore(int scoreDelta){
	totalScore += scoreDelta;
}

void _CreateScoreText(){
	scoreText = agk::CreateText("0");
	agk::SetTextSize(scoreText,18);
	agk::SetTextPosition(scoreText,50.0,55.0);
	agk::SetTextDepth(scoreText,1);
}

void _UpdateScoreText(int score){
	agk::SetTextString(scoreText, format_my_way("%d",score));
}

void _PlayCoinAnimation(){
	agk::PlaySprite(51,10,1);
}

void _StopCoinAnimation(){
	agk::StopSprite(51);
}

void _ResumeCoinAnimation(){
	agk::ResumeSprite(51);
}

void _DeleteCoinAnimation(){
	agk::DeleteSprite(51);
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////MAIN FUNCTIONS////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

void Game::StartGame(){
	CreateEnvironment();
	if(!characterCreated){
		CreateCharacter();
	}
	if(!enemiesCreated){
		CreateEnemies();
	}
	if(characterAlive && enemyCounter > 0){
		if(!gamePaused){
			UpdateCharacterPositions();
			UpdateEnemyPositions();
		}else{
			PauseGame();
		}
	}else if(characterAlive && enemyCounter == 0){
		DestroyEnvironment();
		DestroyEnemy();
		agk::SetSpriteVisible(character.sprite,0);
		//Show tip
		NextLevel();
		//Next level menu
		if(agk::GetVirtualButtonPressed(9) == 1){
			agk::DeleteVirtualButton(9);
			nextLevelMenuCreated = false;
			DestroyCharacter();
			MainReset();
			enemiesCreated = false;
			characterCreated = false;
			level ++;
			CreateEnvironment();
			CreateCharacter();
			CreateEnemies();
		}
	}
	else if(!characterAlive && enemyCounter > 0){
		g_GameMode = 0;
		DestroyCharacter();
		_DeleteCoinAnimation();
		DestroyEnemy();
		DestroyEnvironment();
		enemiesCreated = false;
		characterCreated = false;
		MainReset();
	}
	if(agk::GetVirtualButtonPressed(1)==1){
		gamePaused = true;
	}
}

void Game::StopGame(){
}

void Game::PauseGame(){
	//Pause settings for character
	if(!charSpeedSet){
		character.pauseSpeedX = character.currentSpeedX;
		character.pauseSpeedY = character.currentSpeedY;
		charSpeedSet = true;
	}
	character.currentSpeedX = 0.0f;
	character.currentSpeedY = 0.0f;
	//freeze enemies
	for(int i = 0; i<enemiesSprites.size(); ++i){
		Enemy enemy = enemiesSprites.at(i);
		if(enemy.isAlive){
			enemy.pauseSpeedX = enemy.currentSpeedX;
			enemy.pauseSpeedY = enemy.currentSpeedY;
			enemy.pauseDirectionX = enemy.directionX;
			enemy.pauseDirectionY = enemy.directionY;
			enemy.pauseRotationSpeed = enemy.rotationSpeed;
			enemy.currentSpeedX = 0.0f;
			enemy.currentSpeedY = 0.0f;
			enemy.rotationSpeed = 0.0f;
		}
	}
	//hide environment
	agk::SetVirtualJoystickVisible(1,0);
	agk::SetVirtualButtonVisible(1,0);
	if(agk::GetVirtualButtonState(10)==1){
		agk::SetVirtualButtonVisible(10,0);
	}
	agk::SetSpriteVisible(2,0);
	agk::SetSpriteVisible(3,0);
	agk::SetTextVisible(1,0);
	agk::SetTextVisible(2,0);
	_StopCoinAnimation();
	//show notification and resume button
	if(!resumeCreated){
		agk::CreateText(3,"Game Paused");
		agk::SetTextSize(3, 50);
		agk::SetTextPosition(3,(agk::GetDeviceWidth()-300)/2,agk::GetDeviceHeight()/2 - 100);
		agk::AddVirtualButton(9,agk::GetDeviceWidth()/2,agk::GetDeviceHeight()/2 + 80, 60);
		agk::SetVirtualButtonAlpha(9,50);
		agk::SetVirtualButtonImageDown(9,ResumeButtonImage);
		agk::SetVirtualButtonImageUp(9,ResumeButtonImage);
		resumeCreated = true;
	}
	if(agk::GetVirtualButtonPressed(9) == 1){
		//Delete resume environ
		agk::DeleteText(3);
		agk::DeleteVirtualButton(9);
		resumeCreated = false;
		_ResumeCoinAnimation();
		agk::SetVirtualJoystickVisible(1,1);
		if(agk::GetVirtualButtonState(10)==1){
			agk::SetVirtualButtonVisible(10,1);
		}
		agk::SetSpriteVisible(2,1);
		agk::SetSpriteVisible(3,1);
		agk::SetTextVisible(1,1);
		agk::SetTextVisible(2,1);
		agk::SetVirtualButtonVisible(1,1);
		//Resume settings for character
		character.currentSpeedX = character.pauseSpeedX;
		character.currentSpeedY = character.pauseSpeedY;
		charSpeedSet = false;
		//resume enemies
		for(int i = 0; i<enemiesSprites.size(); ++i){
			Enemy enemy = enemiesSprites.at(i);
			if(enemy.isAlive){
				enemy.currentSpeedX = enemy.pauseSpeedX;
				enemy.currentSpeedY = enemy.pauseSpeedY;
				enemy.rotationSpeed = enemy.pauseRotationSpeed;
			}
		}
		gamePaused = false;
	}
}

void Game::NextLevel(){
	if(!nextLevelMenuCreated){
		agk::AddVirtualButton(nextLevelButton, 580, 450,60);
		agk::SetVirtualButtonImageDown(nextLevelButton,NextLevelButtonImage);
		agk::SetVirtualButtonImageUp(nextLevelButton,NextLevelButtonImage);
		nextLevelMenuCreated = true;
	}
}

void Game::LoadGameImages(){
	characterImage = agk::LoadImage("Character1.png");
	enemyImage = agk::LoadImage("Enemy.png");
	targetImage = agk::LoadImage("Targeting.png");
	PauseButtonImage = agk::LoadImage("PauseButton.png");
	ResumeButtonImage = agk::LoadImage("ResumeButton.png");
	NextLevelButtonImage = agk::LoadImage("NextLevelButton.png");
	ScoreCoinImage1 = agk::LoadImage("Coin1.png");
	ScoreCoinImage2 = agk::LoadImage("Coin2.png");
	ScoreCoinImage3 = agk::LoadImage("Coin3.png");
	ScoreCoinImage4 = agk::LoadImage("Coin4.png");
	ScoreCoinImage5 = agk::LoadImage("Coin5.png");
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////PRIVATE FUNCTIONS//////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

void Game::CreateCharacter(){
	character.sprite=agk::CreateSprite(characterImage);
	agk::SetSpriteShape(character.sprite,3);
	agk::SetSpritePosition(character.sprite, (float)agk::GetDeviceWidth()/2 - 20.0f, (float)agk::GetDeviceHeight()/2 - 20.0f);
	agk::SetSpriteSize(character.sprite,characterWidth,characterHeight);
	agk::SetSpriteColor(character.sprite,0,200,200,255);
	character.currentSpeedX = 0;
	character.currentSpeedY = 0;
	character.isAlive = true;
	characterCreated = true;
	characterAlive = true;
	CreateParticles();
}

void Game::CreateEnemies(){
	if(level > 1){
		enemyCounter = enemyCounter + level * 2;
	}
	for(int i = 0; i < enemyCounter; ++i){
		Game::enemy.sprite = agk::CreateSprite(enemyImage);
		agk::SetSpriteSize(Game::enemy.sprite,(float)agk::Random(20.0f,60.0f),-1);
		agk::SetSpriteColor(Game::enemy.sprite, agk::Random(0,255),agk::Random(0,255), agk::Random(0,255), 255);
		agk::SetSpritePosition(Game::enemy.sprite,(float)agk::Random(0.0f,620.0f),(float)agk::Random(0.0f,480.0f));
		agk::SetSpriteShape(Game::enemy.sprite,3);
		agk::SetSpriteUVBorder(Game::enemy.sprite,5.0f);
		if(agk::GetSpriteX(Game::enemy.sprite) <= agk::GetSpriteX(character.sprite) + 30){
			agk::SetSpriteX(Game::enemy.sprite, agk::GetSpriteX(character.sprite) + 100);
		}else if(agk::GetSpriteX(Game::enemy.sprite) >= agk::GetSpriteX(character.sprite) - 30){
			agk::SetSpriteX(Game::enemy.sprite, agk::GetSpriteX(character.sprite) - 100);
		}else if(agk::GetSpriteY(Game::enemy.sprite) <= agk::GetSpriteY(character.sprite) + 30){
			agk::SetSpriteY(Game::enemy.sprite, agk::GetSpriteY(character.sprite) + 100);
		}else if(agk::GetSpriteY(Game::enemy.sprite) >= agk::GetSpriteY(character.sprite) - 30){
			agk::SetSpriteY(Game::enemy.sprite, agk::GetSpriteY(character.sprite) - 100);
		}
		Game::enemy.rotationSpeed = (float)(agk::Random(0.0f,9.0f)/10.0f);
		Game::enemy.currentSpeedX = (float)(agk::Random(0.0f,2.0f) / 10.0f) * (int)agk::Round(agk::Random(0,1)) * 2 - 1;
		Game::enemy.currentSpeedY = (float)(agk::Random(0.0f,2.0f) / 10.0f) * (int)agk::Round(agk::Random(0,1)) * 2 - 1;
		Game::enemy.directionX = (int)agk::Round(agk::Random(0,1)) * 2 - 1;
		Game::enemy.directionY = (int)agk::Round(agk::Random(0,1)) * 2 - 1;
		Game::enemy.isAlive = true;
		enemiesSprites.push_back(Game::enemy);
	}
	smallestSprite = enemiesSprites.at(0).sprite;
	agk::CreateSprite(103,targetImage);
	agk::SetSpriteSize(103,agk::GetSpriteWidth(smallestSprite), agk::GetSpriteHeight(smallestSprite));
	agk::SetSpriteColorAlpha(103,80);
	agk::SetSpriteDepth(103,1);
	agk::SetSpritePosition(103,agk::GetSpriteX(smallestSprite), agk::GetSpriteY(smallestSprite));
	enemiesCreated = true;
}

void Game::CreateEnvironment(){
	if(!joystickCreated){
		agk::AddVirtualJoystick(1,80,400,80);
		agk::CreateSprite(2,0);
		agk::SetSpriteSize(2,80,10);
		agk::SetSpritePosition(2,150, 10);
		agk::SetSpriteColor(2,agk::GetSpriteColorRed(character.sprite), agk::GetSpriteColorGreen(character.sprite), agk::GetSpriteColorBlue(character.sprite), 255);
		agk::SetSpriteDepth(2,1);
		agk::CreateSprite(3,0);
		agk::SetSpriteSize(3, 5, 10);
		agk::SetSpritePosition(3, 150, 30);
		agk::SetSpriteColorAlpha(3,80);
		agk::SetSpriteDepth(3,1);
		agk::CreateText(1, "Player:");
		agk::CreateText(2, "Smallest cell:");
		agk::SetTextSize(1,14);
		agk::SetTextSize(2,14);
		agk::SetTextPosition(1,10,10);
		agk::SetTextPosition(2,10,30);
		agk::AddVirtualButton(1,agk::GetDeviceWidth()/2, 450, 50);
		agk::SetVirtualButtonAlpha(1,50);
		agk::SetVirtualButtonImageUp(1,PauseButtonImage);
		agk::SetVirtualButtonImageDown(1,PauseButtonImage);
		_CreateScoreAnimation();
		_PlayCoinAnimation();
		joystickCreated = true;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////UPDATING POSITIONS////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Game::UpdateCharacterPositions(){
	if(joystickCreated){
		agk::SetSpriteSize(2,(float)80.0 * (float)(agk::GetSpriteWidth(character.sprite)/40), agk::GetSpriteHeight(2));
		agk::SetSpriteColor(2,agk::GetSpriteColorRed(character.sprite), agk::GetSpriteColorGreen(character.sprite), agk::GetSpriteColorBlue(character.sprite), 255);
		agk::SetParticlesDirection     ( 1, agk::GetVirtualJoystickX(1) * 50.0 * -1,agk::GetVirtualJoystickY(1) * 50.0 * -1 );
		agk::SetParticlesStartZone(1,agk::GetSpriteX(character.sprite) + agk::GetSpriteWidth(character.sprite)/4,agk::GetSpriteY(character.sprite)+ agk::GetSpriteWidth(character.sprite)/4,agk::GetSpriteX(character.sprite) + agk::GetSpriteWidth(character.sprite)*3/4,agk::GetSpriteY(character.sprite) + agk::GetSpriteWidth(character.sprite)*3/4);
		agk::SetParticlesLife(1,0.5 + agk::GetSpriteWidth(character.sprite)/200);
		if(agk::GetVirtualJoystickX(1) > 0){
			character.currentSpeedX += 0.01;
			characterMoving = true;
			if(character.currentSpeedX >= 0.8f){
				character.currentSpeedX = 0.8f;
			}
		}else if(agk::GetVirtualJoystickX(1) < 0){
			character.currentSpeedX -= 0.01;
			characterMoving = true;
			if(character.currentSpeedX <= -0.8f){
				character.currentSpeedX = -0.8f;
			}
		}else if(agk::GetVirtualJoystickX(1) == 0){
			character.currentSpeedX = character.currentSpeedX / 1.01;
		}
		if(agk::GetVirtualJoystickY(1) > 0){
			character.currentSpeedY += 0.01;
			characterMoving = true;
			if(character.currentSpeedY >= 0.8f){
				character.currentSpeedY = 0.8f;
			}
		}else if(agk::GetVirtualJoystickY(1) < 0){
			character.currentSpeedY -= 0.01;
			characterMoving = true;
			if(character.currentSpeedY <= -0.8f){
				character.currentSpeedY = -0.8f;
			}
		}else if(agk::GetVirtualJoystickY(1) == 0){
			character.currentSpeedY = character.currentSpeedY / 1.01;
		}
		if(agk::GetVirtualJoystickX(1) == 0 && agk::GetVirtualJoystickY(1) == 0){
			characterMoving = false;
		}
		if(characterMoving){
			agk::SetParticlesVisible(1,1);
			agk::SetSpriteSize(character.sprite,agk::GetSpriteWidth(character.sprite) - 0.01, agk::GetSpriteHeight(character.sprite) - 0.01);
			agk::SetSpriteSize(2,(float)80.0 * (float)(agk::GetSpriteWidth(character.sprite)/40), agk::GetSpriteHeight(2));
		}else{
			agk::SetParticlesVisible(1,0);
		}
		agk::SetSpritePosition(character.sprite,agk::GetSpriteX(character.sprite) + character.currentSpeedX, agk::GetSpriteY(character.sprite) + character.currentSpeedY);
		if(agk::GetSpriteX(character.sprite) > 620 + agk::GetSpriteWidth(character.sprite)/2){
			agk::SetSpriteX(character.sprite,0 - agk::GetSpriteWidth(1));
		}else if(agk::GetSpriteX(character.sprite) < 0 - agk::GetSpriteWidth(character.sprite)){
			agk::SetSpriteX(character.sprite,620);
		}
		if(agk::GetSpriteY(character.sprite) > 480 ){
			agk::SetSpriteY(character.sprite, 0 - agk::GetSpriteHeight(character.sprite));
		}else if(agk::GetSpriteY(character.sprite) < 0 - agk::GetSpriteHeight(character.sprite)){
			agk::SetSpriteY(character.sprite,480);
		}
		if(agk::GetSpriteWidth(character.sprite) > 50){
			weapon.CreateFireButton();
			if(agk::GetVirtualButtonPressed(10)==1){
				agk::SetSpriteSize(character.sprite,agk::GetSpriteWidth(character.sprite) - 10, agk::GetSpriteHeight(character.sprite) - 10);
				weapon.weaponFiring = 1;
			}
		}else{
			weapon.DeleteFireButton();
		}
		if(weapon.weaponFiring == 1){
			weapon.FireShrinkingRing(agk::GetSpriteX(character.sprite), agk::GetSpriteY(character.sprite), agk::GetSpriteWidth(character.sprite),agk::GetSpriteHeight(character.sprite));
		}
	}
}

void Game::UpdateEnemyPositions(){
	_FindSmallestSprite();
	agk::SetSpriteSize(3, agk::GetSpriteWidth(smallestSprite) * 2, agk::GetSpriteHeight(3));
	agk::SetSpriteColor(3, agk::GetSpriteColorRed(smallestSprite), agk::GetSpriteColorGreen(smallestSprite), agk::GetSpriteColorBlue(smallestSprite), 255);
	for(int i = 0; i < enemiesSprites.size(); ++i){
		Enemy tempSprite = enemiesSprites.at(i);
		if(agk::GetSpriteX(tempSprite.sprite) >= 620 + agk::GetSpriteWidth(tempSprite.sprite)/2){
			agk::SetSpriteX(tempSprite.sprite,0 - agk::GetSpriteWidth(tempSprite.sprite));
		}else if(agk::GetSpriteX(tempSprite.sprite) < 0 - agk::GetSpriteWidth(tempSprite.sprite)){
			agk::SetSpriteX(tempSprite.sprite,620);
		}
		if(agk::GetSpriteY(tempSprite.sprite) >= 480){
			agk::SetSpriteY(tempSprite.sprite,0 - agk::GetSpriteHeight(tempSprite.sprite));
		}else if(agk::GetSpriteY(tempSprite.sprite) < 0 - agk::GetSpriteHeight(tempSprite.sprite)){
			agk::SetSpriteY(tempSprite.sprite,480);
		}
		if(agk::GetSpriteCollision(character.sprite,tempSprite.sprite)==1 ){
			if(agk::GetSpriteWidth(character.sprite) > agk::GetSpriteWidth(tempSprite.sprite) || enemyCounter == 1){
				agk::SetSpriteSize(character.sprite,agk::GetSpriteWidth(character.sprite) + agk::GetSpriteWidth(tempSprite.sprite)/2, agk::GetSpriteHeight(character.sprite) + agk::GetSpriteHeight(tempSprite.sprite)/2);
				_UpdateScore(agk::Round(agk::GetSpriteWidth(tempSprite.sprite)/2));
				_UpdateScoreText(totalScore);
				agk::DeleteSprite(tempSprite.sprite);
				tempSprite.isAlive = false;
				agk::SetSpriteSize(2,(float)80 * (float)(agk::GetSpriteWidth(character.sprite)/40), agk::GetSpriteHeight(2));
				characterWidth = agk::GetSpriteWidth(character.sprite);
				characterHeight = agk::GetSpriteHeight(character.sprite);
				enemyCounter --;
			}else if(tempSprite.sprite == smallestSprite){
				agk::SetSpriteSize(character.sprite,agk::GetSpriteWidth(character.sprite) + agk::GetSpriteWidth(tempSprite.sprite)/2, agk::GetSpriteHeight(character.sprite) + agk::GetSpriteHeight(tempSprite.sprite)/2);
				_UpdateScore(agk::Round(agk::GetSpriteWidth(tempSprite.sprite)/2));
				_UpdateScoreText(totalScore);
				agk::DeleteSprite(tempSprite.sprite);
				tempSprite.isAlive = false;
				agk::SetSpriteSize(2,(float)80.0 * (float)(agk::GetSpriteWidth(character.sprite)/40), agk::GetSpriteHeight(2));

				enemyCounter --;
			}else{
				DestroyCharacter();
			}
		}
		if(agk::GetSpriteHitTest(tempSprite.sprite,agk::GetPointerX(), agk::GetPointerY())==1){
			agk::Print(tempSprite.sprite);
		}

		if(agk::GetSpriteActive(weapon.explosion) == 1){
			if(agk::GetSpriteCollision(tempSprite.sprite,weapon.explosion) ==1 && agk::GetSpriteWidth(tempSprite.sprite)>20){
				agk::SetSpriteSize(tempSprite.sprite,agk::GetSpriteWidth(tempSprite.sprite) - (float)(25 * (float)(agk::GetSpriteColorAlpha(weapon.explosion)/205)), -1);
			}
			if(agk::GetSpriteWidth(tempSprite.sprite)<20){
				agk::SetSpriteSize(tempSprite.sprite,20,-1);
			}
		}
		agk::SetSpriteSize(103,agk::GetSpriteWidth(smallestSprite) + 15, agk::GetSpriteHeight(smallestSprite) + 15);
		agk::SetSpritePosition(103,agk::GetSpriteX(smallestSprite) - 7.5, agk::GetSpriteY(smallestSprite)-7.5);
		agk::SetSpriteAngle(tempSprite.sprite,agk::GetSpriteAngle(tempSprite.sprite) + tempSprite.rotationSpeed * tempSprite.directionX * tempSprite.directionY);
		agk::SetSpriteX(tempSprite.sprite,(float)agk::GetSpriteX(tempSprite.sprite) + (float)tempSprite.currentSpeedX /10 * tempSprite.directionX);
		agk::SetSpriteY(tempSprite.sprite,(float)agk::GetSpriteY(tempSprite.sprite) + (float)tempSprite.currentSpeedY /10 * tempSprite.directionY);
	}
	_CheckCollision();
}

void Game::DestroyCharacter(){
	agk::DeleteSprite(character.sprite);
	characterAlive = false;
}

void Game::DestroyEnemy(){
	for(int i = 0; i < enemiesSprites.size(); i++){
		Enemy temp = enemiesSprites.at(i);
		agk::DeleteSprite(temp.sprite);
	}
	enemiesSprites.erase(enemiesSprites.begin(),enemiesSprites.end());
	enemiesSprites.clear();
	agk::DeleteSprite(103);
}

void Game::DestroyEnvironment(){
	agk::DeleteVirtualJoystick(1);
	agk::DeleteVirtualButton(1);
	if(agk::GetParticlesActive(1)==1){
		agk::DeleteParticles(1);
		particleCreated = false;
	}
	agk::DeleteSprite(2);
	agk::DeleteSprite(3);
	agk::DeleteText(1);
	agk::DeleteText(2);
	weapon.DeleteFireButton();
	_DeleteCoinAnimation();
	agk::DeleteText(scoreText);
	joystickCreated = false;
}

void Game::MainReset(){
	character.currentSpeedX = 0.0;
	character.currentSpeedY = 0.0;
	totalScore = 0;
	characterWidth = 40.0f;
	characterHeight = 40.0f;
	enemyCounter = 10;
	level = 1;
}

void Game::CreateParticles(){
	if(!particleCreated){
		agk::LoadImage(1,"particle.png");
		agk::CreateParticles(1,0, 0);
		agk::SetParticlesImage(1,1);
		agk::SetParticlesStartZone(1,agk::GetSpriteX(character.sprite),agk::GetSpriteY(character.sprite),agk::GetSpriteX(character.sprite) + agk::GetSpriteWidth(character.sprite),agk::GetSpriteY(character.sprite) + agk::GetSpriteWidth(character.sprite));
		agk::SetParticlesDirection     ( 1, agk::GetVirtualJoystickX(1) * 50.0,agk::GetVirtualJoystickY(1) * 50.0 );
		agk::SetParticlesLife          ( 1, 0.5 );
		agk::SetParticlesSize          ( 1, 5 );
		agk::SetParticlesAngle         ( 1, 15 );
		agk::SetParticlesFrequency     ( 1, 100 );
		agk::SetParticlesVelocityRange ( 1, 1, 4 );
		agk::AddParticlesColorKeyFrame ( 1, 0, 0, 100, 255, 0 );
		agk::AddParticlesColorKeyFrame ( 1, 0.1, 0, 100, 255, 255 );
		agk::AddParticlesColorKeyFrame ( 1, 0.4, 150, 50, 100, 255 );
		agk::AddParticlesColorKeyFrame ( 1, 0.7, 250, 50, 10, 100 );
		agk::SetParticlesDepth(1,3);
		particleCreated = true;
	}
}
