#pragma once
class Menu
{

public:
	void LoadMenuButtonImages(void);
	void LoadBackgroundImages(void);
	void ChangeBackgroundImage(void);
	void CreateSettingsMenu(void);
	void CreateMainMenu(void);
	void CreatePlayMenu(void);
	void CreateAchievmentMenu(void);
	void CreateDialogMenu(void);
	void DeleteAchievmentMenu(void);
	void DeleteMainMenu(void);
	void DeletePlayMenu(void);
	void DeleteSettingMenu(void);
	void DeleteDialogMenu(void);
};

